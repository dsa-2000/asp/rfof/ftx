# DSA2000 Fiber Transmitter

This repository contains the hardware design for the RF over fiber transmitter module.
This module provides the antenna-local analog signal processing post-LNA and drives the laser of the RF over fiber link.
Additionally, this module provides several monitor and control points that will eventually be integrated into the telescope's global monitor and control system.

## Hardware

Designed using KiCad 8, and requires setup of our [library](https://gitlab.com/dsa-2000/asp/kicad-library).

### Artifacts

As part of the CI/CD pipeline for this project, schematics, board files, BoMs, etc. are rendered on each change.
You can find these [here](https://gitlab.com/dsa-2000/asp/rfof/ftx/-/artifacts)
